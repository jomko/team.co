#= require_tree ./vendor
#= require_tree ./lib

(function() {

	$(".menu li").on("click", "a", function(e){
		e.preventDefault();
		var current = $(this).parent().attr("class");
		$(".menu li a").removeClass('current');
		$(this).addClass('current');
		$(".content section").removeClass('active');
		$(".content section#"+current).addClass('active');
	});

	$("#how li").on("click", "img", function(e){
		$(this).parent().addClass('expanded');
	});
	$("#how li").on("click", "span.close", function(e){
		console.log("here");
		$("#how li").removeClass('expanded');
	});	

		//Convert floats to absolute
		// $("#how ul li").each(function(){
		// 		// var pos = $(this).position();
		// 		// console.log(pos);
		// 		// $top = Math.floor(Number(pos.top)) + 'px';
		// 		// $left = Math.floor(Number(pos.left)) + 'px';
		// 		$(this).css({position: 'absolute', left: $(this).position().left, top: $(this).position().top});			
		// });	

}).call(this);